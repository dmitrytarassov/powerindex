import styled from "styled-components";

const Card = styled.div`
  background: #fff;
  border-radius: 10px;
  padding: 10px;
  display: flex;
  align-items: center;
  font-family: Gilroy;
  
  & + & {
    margin-top: 10px;
  }
`;

export default Card;