import React from 'react';
import Logo from "./Logo.svg";
import styled from "styled-components";
import {Button} from "@material-ui/core";

const HeaderComponent = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  width: 100%;
  margin-bottom: 20px;
`;

const Links = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
`;

const Link = styled.a`
   margin-right: 10px;
   color: #000000;
   text-decoration: none;
   :hover {
      color: #AB4CEE;
   }
`;

export default function Header() {
  return (
    <HeaderComponent>
      <a href="/">
        <img src={Logo} title="Powerindex" alt="Powerindex" />
      </a>
      <Links>
        <Link href="https://powerindex.io/#/mainnet/farming/">Farming</Link>
        <Link href="https://powerindex.io/#/mainnet/">Pools</Link>
        <Button
          variant="contained"
          href="https://app.powerpool.finance/"
          color="primary"
        >
          PRO ver
        </Button>
      </Links>
    </HeaderComponent>
  );
};