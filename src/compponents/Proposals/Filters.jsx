import React from 'react';
import styled from "styled-components";
import { Select, MenuItem, InputLabel, FormControl, TextField } from '@material-ui/core';

import Card from "../Card";

const StyledCard = styled(Card)`
  justify-content: space-between;
  margin-bottom: 10px;
  @media (max-width: 750px) {
    flex-wrap: wrap;
  }
`;

const StyledFormControl = styled(FormControl)`
  width: 24%;
  
  @media (max-width: 750px) {
    width: 49%;
    :nth-child(3), :nth-child(4) {
      margin-top: 10px;
    }
  }
`

const Filters = ({ indexes, protocols, statuses, filterValues, onChange }) => {

  const onChangeHandler = (key) => (e) => {
    const { value } = e.target;
    onChange({
      [key]: value
    });
  };

  return (
    <StyledCard>
      <StyledFormControl variant="outlined">
        <InputLabel>Index</InputLabel>
        <Select label={"Index"} onChange={onChangeHandler('index')} value={filterValues.selectedIndex}>
          <MenuItem value={"all"}>All</MenuItem>
          { indexes.map((index) => <MenuItem value={index} key={index}>
            { index }
          </MenuItem>)}
        </Select>
      </StyledFormControl>
      <StyledFormControl variant="outlined">
        <InputLabel>Protocol</InputLabel>
        <Select label={"Protocol"} onChange={onChangeHandler('protocol')} value={filterValues.selectedProtocol}>
          <MenuItem value={"all"}>All</MenuItem>
          { protocols.map((protocol) => <MenuItem value={protocol} key={protocol}>
            { protocol }
          </MenuItem>)}
        </Select>
      </StyledFormControl>
      <StyledFormControl variant="outlined">
        <InputLabel>State</InputLabel>
        <Select label={"State"} onChange={onChangeHandler('status')} value={filterValues.selectedStatus}>
          <MenuItem value={"all"}>Any</MenuItem>
          { statuses.map((status) => <MenuItem value={status} key={status}>
            { status }
          </MenuItem>)}
        </Select>
      </StyledFormControl>
      <StyledFormControl variant="outlined">
        <TextField placeholder={"Search"} value={filterValues.searchValue} variant="outlined" onChange={onChangeHandler("search")} />
      </StyledFormControl>
    </StyledCard>
  );
};

Filters.propTypes = {

};

export default Filters;