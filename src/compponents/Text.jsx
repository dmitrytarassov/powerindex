import styled from "styled-components";

export default styled.div`
  text-align: center;
  width: 100%;
  color: rgba(36,36,36,.8);
  margin-bottom: 40px;
  font-family: Gilroy;  
`;