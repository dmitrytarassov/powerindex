import React from 'react';
import styled from "styled-components";
import info from "./attention.svg";
import Card from "../Card";

const getIcon = (type) => ({
  info,
}[type] || info);

const Container = styled.div`
  display: flex;
  flex-direction: column;
  margin-bottom: 30px;
  width: 100%;
`;

const Icon = styled.img`
  width: 28px;
  height: 28px;
  margin-right: 10px;
`;

const Index = ({ messages }) => {
  return (
    <Container>
      {
        messages.map((message) => <Card key={message.text}>
          <Icon src={getIcon(message.type)} alt={message.text} title={message.text} />
          { message.text }
        </Card>)
      }
    </Container>
  );
};

export default Index;