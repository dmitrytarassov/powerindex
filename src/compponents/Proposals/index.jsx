import React from 'react';
import styled from "styled-components";
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TableContainer from '@material-ui/core/TableContainer';
import { withStyles } from '@material-ui/core/styles';
import Button from "@material-ui/core/Button";
import CircularProgress from "@material-ui/core/CircularProgress";

import Card from "../Card";
import Filters from "./Filters";

const StyledTableRow = styled(TableRow)`
  &:last-child {
    td {
      border-bottom: none;
    }
  }
`;

const StyledTableCell = withStyles(() => ({
  head: {
    backgroundColor: "#f5f5f8",
  },
  body: {
    fontSize: 14,
  },
}))(TableCell);

const Protocol = styled.div`
  display: flex;
  align-items: center;
  justify-content: flex-end;
  
  img {
    margin-right: 10px;
    height: 24px;
  }
`;

const Indexes = styled.div`
  display: flex;
  align-items: center;
  justify-content: flex-end;
  
  img {
    height: 24px;
    margin-left: 5px;
  }
`;

const ProcessContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
`;

const Proposals = ({ proposals, loading }) => {
  const [selectedIndex, setSelectedIndex] = React.useState("all");
  const [selectedProtocol, setSelectedProtocol] = React.useState("all");
  const [selectedStatus, setSelectedStatus] = React.useState("all");
  const [searchValue, setSearchValue] = React.useState("");

  const indexes = [...new Set(proposals.map(({ indexes }) => indexes).flat())];
  const protocols = [...new Set(proposals.map(({ protocol }) => protocol.name))];
  const statuses = ["Active", "Closed"];

  let filteredProposals = [...proposals];
  if (selectedIndex && selectedIndex !== "all") {
    filteredProposals = filteredProposals.filter(({ indexes }) => indexes.includes(selectedIndex));
  }
  if (selectedProtocol && selectedProtocol !== "all") {
    filteredProposals = filteredProposals.filter(({ protocol }) => protocol.name === selectedProtocol);
  }
  if (selectedStatus && selectedStatus !== "all") {
    const status = selectedStatus === "Active";
    filteredProposals = filteredProposals.filter(({ active }) => active === status);
  }
  if (searchValue) {
    filteredProposals = filteredProposals.filter(({ name }) => name.toLowerCase().includes(searchValue.toLowerCase()));
  }

  const filterValues = {
    selectedIndex,
    selectedProtocol,
    selectedStatus,
    searchValue,
  };

  const onFilterChange = (values) => {
    if (typeof values.index !== "undefined") setSelectedIndex(values.index);
    if (typeof values.protocol !== "undefined") setSelectedProtocol(values.protocol);
    if (typeof values.status !== "undefined") setSelectedStatus(values.status);
    if (typeof values.search !== "undefined") setSearchValue(values.search);
  }

  const indexesLogos = {
    "Power Index": 'https://powerindex.io/images/index-mining/pipt.svg',
    "ASSY Index": 'https://powerindex.io/images/index-tokens/assy.svg',
    "Yearn Ecosystem": 'https://powerindex.io/images/index-mining/yeti.svg',
    "Yearn Lazy Ape Index": 'https://powerindex.io/images/index-tokens/yla.svg',
  }

  return (
    <div>
      <Filters filterValues={filterValues} onChange={onFilterChange} indexes={indexes} protocols={protocols} statuses={statuses} />
      <Card>
        <TableContainer>
          <Table>
            <TableHead>
              <StyledTableRow>
                <StyledTableCell>Name</StyledTableCell>
                <StyledTableCell align="right">Indexes</StyledTableCell>
                <StyledTableCell align="right">Protocol</StyledTableCell>
                <StyledTableCell align="right">State</StyledTableCell>
                <StyledTableCell align="right">Actions</StyledTableCell>
              </StyledTableRow>
            </TableHead>
            <TableBody>
              { loading && <StyledTableRow>
                <StyledTableCell colSpan={5}>
                  <ProcessContainer>
                    <CircularProgress />
                  </ProcessContainer>
                </StyledTableCell>
              </StyledTableRow> }
              { !loading && filteredProposals.map(p => <StyledTableRow key={p._id}>
                <StyledTableCell>{p.name}</StyledTableCell>
                <StyledTableCell align="right">
                  <Indexes>
                    {
                      p.indexes.map(i => indexesLogos[i] ? <img key={i} src={indexesLogos[i]} alt={i} /> : null)
                    }
                  </Indexes>
                </StyledTableCell>
                <StyledTableCell align="right">
                  <Protocol>
                    <img src={p.protocol.logo} alt={p.protocol.name} title={p.protocol.name} />
                    { p.protocol.name }
                  </Protocol>
                </StyledTableCell>
                <StyledTableCell align="right">
                  { p.active ? "Live" : "Closed" }
                </StyledTableCell>
                <StyledTableCell align="right">
                  <Button
                    variant="contained"
                    color={ !p.active ? "primary" : "secondary" }
                    href={p.link || "https://snapshot.powerpool.finance/#/"}
                    target="_blank"
                  >{ p.active ? "Vote" : "View" }</Button>
                </StyledTableCell>
              </StyledTableRow>) }
            </TableBody>
          </Table>
        </TableContainer>
      </Card>
    </div>
  );
};

export default Proposals;