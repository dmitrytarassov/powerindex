import React from 'react';
import Layout from "./compponents/Layout";
import Header from "./compponents/Header";
import Flash from "./compponents/Flash";
import Proposals from "./compponents/Proposals";
import { ThemeProvider, createMuiTheme } from '@material-ui/core/styles';

import H1 from "./compponents/H1";
import Text from "./compponents/Text";
import axios from "axios";

const theme = createMuiTheme({
  overrides: {
    MuiButton: {
      containedPrimary: {
        backgroundColor: '#EDDAFA',
        color: "#AB4CEE",
        '&:hover': {
          backgroundColor: '#EDDAFA',
        }
      },
      containedSecondary: {
        backgroundColor: '#E1F2DC',
        color: "#73C259",
        '&:hover': {
          backgroundColor: '#E1F2DC',
        }
      },
    }
  }
});


const flashMessages = [{
  type: 'info',
  text: 'PowerPool has passed several public security audits. Still, It is experimental software. Please, use it at your own risk.'
}];

function App() {
  const [data, setData] = React.useState([]);
  const [loading, setLoading] = React.useState(true);

  React.useEffect(() => {
    const url = "https://sheets.googleapis.com/v4/spreadsheets/1sXslCnrUaRV7HYUsElI5___6Ub35y1ttEKvObQCw6fU/?key=AIzaSyCrtPy5CYGFwSdh_3CH5BZ39NgHbP45AeE&includeGridData=true";

    let delay = 1000;

    setTimeout(() => {
      delay = 0;
    }, delay);

    axios.get(url)
      .then(function (response) {
        const data = response.data.sheets[0].data[0].rowData.map((row, i) => {
          if (i > 0) {
            if (row && row.values && row.values[0].formattedValue) {
              const { values } = row;
              return {
                _id: Math.random(),
                name: values[0].formattedValue,
                active: values[1].formattedValue === "TRUE",
                indexes: [
                  values[2].formattedValue === "TRUE" ? "Power Index" : "",
                  values[3].formattedValue === "TRUE" ? "ASSY Index" : "",
                  values[4].formattedValue === "TRUE" ? "Yearn Ecosystem" : "",
                  values[5].formattedValue === "TRUE" ? "Yearn Lazy Ape Index" : "",
                ].filter(Boolean),
                protocol: {
                  name: values[6].formattedValue,
                  logo: values[7].formattedValue
                },
                link: values[8].formattedValue
              }
            }
          }
          return false;
        }).filter(Boolean);
        setTimeout(() => {
          setData(data);
          setLoading(false);
        }, delay);
      })
      .catch(function () {
        setLoading(false);
      });
  }, []);

  return (
    <ThemeProvider theme={theme}>
      <Layout>
        <Header />
        <Flash messages={flashMessages} />
        <H1>Meta-Governance Portal</H1>
        <Text>Use CVP to vote in ecosystem protocols</Text>
        <Proposals proposals={data} loading={loading} />
      </Layout>
    </ThemeProvider>
  );
}

export default App;
