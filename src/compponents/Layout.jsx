import React from 'react';
import styled from "styled-components";

const LayoutComponent = styled.div`
  padding: 30px 65px;
  max-width: 1450px;
  margin: auto;
  position: relative;
  
  @media (max-width: 750px) {
    padding: 5px;
  }
`;

export default function Layout ({ children }) {
  return <LayoutComponent>{ children }</LayoutComponent>
};