import styled from "styled-components";

export default styled.h1`
  font-size: 24px;
  text-align: center;
  width: 100%;
  margin: 0 0 30px;
  font-weight: 100;
`;